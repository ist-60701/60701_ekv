from typing import List, Optional

import uvicorn
from fastapi import FastAPI, HTTPException
from sqlmodel import Field, SQLModel, select, create_engine, Session
from starlette import status

connect_args = {"check_same_thread": False}
engine = create_engine("sqlite:///mysimple.db", echo=True,
                       connect_args=connect_args)

app = FastAPI()


class MyBase(SQLModel):
    name: str
    surname: str
    login: str
    subscription_up: str
    status: str


class User(MyBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: Optional[str] = Field(default=None, primary_key=False)
    surname: Optional[str] = Field(default=None, primary_key=False)
    login: Optional[str] = Field(default=None, primary_key=False)
    subscription_up: Optional[str] = Field(default=None, primary_key=False)
    status: Optional[str] = Field(default=None, primary_key=False)


class SomeGet(MyBase):
    id: int
    name: str
    surname: str
    login: str
    subscription_up: str
    status: str


class SomeCreate(MyBase):
    name: str
    surname: str
    login: str
    subscription_up: str
    status: str


class SomeUpdate(MyBase):

    pass


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


@app.on_event("startup")
def on_startup():
    create_db_and_tables()


@app.get("/Some", response_model=List[User])  # 127.0.0.1:8080/Some
def get_User() -> List[User]:
    with Session(engine) as session:
        name: List[User] = session.exec(select(User)).all()
    return name


@app.post("/Some", status_code=201)
def add_User(user: SomeCreate):
    with Session(engine) as session:
        mysimple = User.from_orm(user)
        session.add(mysimple)
        session.commit()
        session.refresh(mysimple)
        return mysimple


@app.patch("/Some/{Some_id}", status_code=status.HTTP_200_OK)
def update_User(Some_id: int, name: SomeUpdate):
    with Session(engine) as session:
        mysimple = session.get(User, Some_id)
        if not mysimple:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="List not found")
        mysimple.name = name.name
        mysimple.surname = name.surname
        mysimple.login = name.login
        mysimple.subscription_up = name.subscription_up
        mysimple.status = name.status
        session.add(mysimple)
        session.commit()
        session.refresh(mysimple)
        return mysimple


@app.delete("/Some/{Some_id}")
def delete_User(Some_id: int):
    with Session(engine) as session:
        name = session.get(User, Some_id)
        if not name:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
        session.delete(name)
        session.commit()
        return name


@app.get("/Some/{Some_id}")
def get_User(Some_id: int) -> Optional[User]:
    with Session(engine) as session:
        name = session.get(User, Some_id)
        if not name:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
        return name


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8080)  # 127.0.0.1:8080
